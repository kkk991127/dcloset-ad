import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_LIST]: (store) => {
        return axios.get(apiUrls.DO_MEMBER_LIST)
    },

    [Constants.DO_MEMBER_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER_DETAIL.replace('{id}', payload.id))
    },



}
