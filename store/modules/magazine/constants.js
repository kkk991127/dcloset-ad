export default {
    DO_MAGAZINE_LIST: '/magazine/doList',
    DO_MAGAZINE_LIST_PAGING: 'test-data/doListPaging',
    DO_MAGAZINE_DETAIL: '/magazine/detail/id',
    DO_MAGAZINE_CREATE_COMMENT: 'test-data/doCreateComment',
    DO_MAGAZINE_UPDATE: 'test-data/doUpdate',
    DO_MAGAZINE_DELETE: 'test-data/doDelete',
    DO_MAGAZINE_CREATE: 'test-data/doCreate'
}
