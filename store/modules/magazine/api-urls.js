const BASE_URL = '/v1/magazine'

export default {
    DO_MAGAZINE_LIST: `${BASE_URL}/all`, //get
    DO_MAGAZINE_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_MAGAZINE_DETAIL: `${BASE_URL}/{id}`, //get
    DO_MAGAZINE_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_MAGAZINE_UPDATE: `${BASE_URL}/{id}`, //put
    DO_MAGAZINE_DELETE: `${BASE_URL}/{id}`, //del
    DO_MAGAZINE_CREATE: `${BASE_URL}/new`, //post
}
