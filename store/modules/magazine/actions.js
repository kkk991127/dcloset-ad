import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MAGAZINE_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_MAGAZINE_LIST_PAGING]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchName.length >= 1) paramArray.push(`searchName=${payload.params.searchName}`)
        if (payload.params.searchAge != null) paramArray.push(`searchAge=${payload.params.searchAge}`)
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_MAGAZINE_LIST_PAGING.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    },
    [Constants.DO_MAGAZINE_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_MAGAZINE_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_MAGAZINE_CREATE_COMMENT]: (store, payload) => {
        return axios.post(apiUrls.DO_MAGAZINE_CREATE_COMMENT.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_MAGAZINE_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_MAGAZINE_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_MAGAZINE_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_MAGAZINE_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_MAGAZINE_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_MAGAZINE_CREATE, payload)
    },
}
